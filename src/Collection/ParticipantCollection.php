<?php

namespace ScoRugby\CaldendrierBundle\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use ScoRugby\CalendrierBundle\Entity\Participant;

/**
 * Description of ParticipantCollection
 *
 * @author Antoine BOUET
 */
class ParticipantCollection extends ArrayCollection {

    public function reponsesPresent(): ArrayCollection {
        return $this->filter(function (Participant $participant) {
                    if (Participant::REPONSE_PRESENT == $participant->getReponse()) {
                        return $participant;
                    }
                });
    }

    public function reponsesAbsent(): ArrayCollection {
        return $this->filter(function (Participant $participant) {
                    if (Participant::REPONSE_ABSENT == $participant->getReponse()) {
                        return $participant;
                    }
                });
    }

    public function reponsesIndecis(): ArrayCollection {
        return $this->filter(function (Participant $participant) {
                    if (Participant::REPONSE_INDECIS == $participant->getReponse()) {
                        return $participant;
                    }
                });
    }

    public function sansReponses(): ArrayCollection {
        return $this->filter(function (Participant $participant) {
                    if (null == $participant->getReponse()) {
                        return $participant;
                    }
                });
    }

    public function presencesEffectives() {
        return $this->filter(function (Participant $participant) {
                    if ($participant->getPresence()) {
                        return $participant;
                    }
                });
    }

    public function absencesEffectives() {
        return $this->filter(function (Participant $participant) {
                    if (!$participant->getPresence()) {
                        return $participant;
                    }
                });
    }

    public function nonExcuses() {
        return $this->filter(function (Participant $participant) {
                    if (null === $participant->getReponse() & !$this->getPresence()) {
                        return $participant;
                    }
                });
    }

    public function absencesPresents() {
        return $this->filter(function (Participant $participant) {
                    if (Participant::REPONSE_ABSENT === $participant->getReponse() & $this->getPresence()) {
                        return $participant;
                    }
                });
    }

    public function presencesAbsents() {
        return $this->filter(function (Participant $participant) {
                    if (Participant::REPONSE_PRESENT === $participant->getReponse() & !$this->getPresence()) {
                        return $participant;
                    }
                });
    }

    public function indecisPresents() {
        return $this->filter(function (Participant $participant) {
                    if (Participant::REPONSE_INDECIS === $participant->getReponse() & $this->getPresence()) {
                        return $participant;
                    }
                });
    }

    public function indecisAbsents() {
        return $this->filter(function (Participant $participant) {
                    if (Participant::REPONSE_INDECIS === $participant->getReponse() & !$this->getPresence()) {
                        return $participant;
                    }
                });
    }
}
