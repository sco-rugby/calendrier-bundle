<?php

namespace ScoRugby\CalendrierBundle\Entity;

use Doctrine\Common\Collections\Collection;
use ScoRugby\CalendrierBundle\Model\Evenement as BaseCalendrier;
use ScoRugby\CaldendrierBundle\Collection\ParticipantCollection;
use ScoRugby\CoreBundle\Entity\SluggifyInterface;
use ScoRugby\CoreBundle\Entity\Slug;

class Evenement extends BaseCalendrier implements SluggifyInterface {

    private ?Calendrier $calendrier = null;
    private Slug $slug;
    private Collection $participants;

    public function __construct() {
        parent::__contruct();
        $this->participants = new ParticipantCollection();
        $this->slug = new Slug();
    }

    public function getSlug(): Slug {
        return $this->slug;
    }

    public function getCalendrier(): ?Calendrier {
        return $this->calendrier;
    }

    public function setCalendrier(?Calendrier $calendrier): static {
        $this->calendrier = $calendrier;

        return $this;
    }

    /**
     * @return Collection<int, Participant>
     */
    public function getParticipants(): Collection {
        return $this->participants;
    }

    public function addParticipant(Participant $participant): static {
        if (!$this->participants->contains($participant)) {
            $this->participants->add($participant);
            $participant->setEvenement($this);
        }

        return $this;
    }

    public function removeParticipant(Participant $participant): static {
        if ($this->participants->removeElement($participant)) {
            // set the owning side to null (unless already changed)
            if ($participant->getEvenement() === $this) {
                $participant->getEvenement(null);
            }
        }

        return $this;
    }
}
