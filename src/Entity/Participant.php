<?php

namespace ScoRugby\CalendrierBundle\Entity;

use ScoRugby\ContactBundle\Entity\Contact;
use ScoRugby\CalendrierBundle\Repository\InviteRepository;
use ScoRugby\CoreBundle\Entity\DateTimeBlameableInterface;
use ScoRugby\CoreBundle\Entity\DateTimeBlameable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Symfony\Component\Uid\Uuid;

//[ORM\Entity(repositoryClass: InviteRepository::class)]
//[ORM\HasLifecycleCallbacks]
//#[ORM\Table(schema: "evenement")]
class Participant implements DateTimeBlameableInterface {

    CONST REPONSE_ABSENT = 0;
    CONST REPONSE_PRESENT = 1;
    CONST REPONSE_INDECIS = 2;

    private ?int $id = null;
    private DateTimeBlameable $datetime;
    private ?int $reponse = null;
    private ?bool $presence = null;
    private ?Uuid $token;
    private ?Contact $contact = null;
    private ?Evenement $evenement = null;

    public function __construct() {
        $this->datetime = new DateTimeBlameable();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getToken(): ?Uuid {
        return $this->token;
    }

    public function getDateTime(): DateTimeBlameable {
        return $this->datetime;
    }

    public function getReponse(): ?int {
        return $this->reponse;
    }

    public function setReponsePresent() {
        $this->reponse = self::REPONSE_PRESENT;
    }

    public function setReponseAbsent() {
        $this->reponse = self::REPONSE_ABSENT;
    }

    public function setReponseIndecis() {
        $this->reponse = self::REPONSE_INDECIS;
    }

    public function getPresence(): ?bool {
        return $this->presence;
    }

    public function setPresence(?bool $presence = true): static {
        $this->presence = $presence;

        return $this;
    }

    public function getContact(): ?Contact {
        return $this->contact;
    }

    public function setContact(?Contact $contact): static {
        $this->contact = $contact;

        return $this;
    }

    public function getEvenement(): ?Evenement {
        return $this->evenement;
    }

    public function setEvenement(?Evenement $evenement): static {
        $this->evenement = $evenement;

        return $this;
    }

    //[PrePersist]
    public function generateToken(PrePersistEventArgs $eventArgs) {
        $this->token = Uuid::v5();
    }
}
