<?php

namespace ScoRugby\CalendrierBundle\Entity;

use ScoRugby\CalendrierBundle\Repository\TypeRepository;
use ScoRugby\CalendrierBundle\Model\TypeEvenementInterface;
use ScoRugby\CoreBundle\Entity\AbstractTaxonomie;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[ORM\MappedSuperclass]
#[ORM\Entity(repositoryClass: TypeRepository::class)]
#[ORM\Table(name: "evenement_type")]
#[ORM\UniqueConstraint(name: "unq_evenement_type_slug", columns: ["slug"])]
class Type extends AbstractTaxonomie implements TypeEvenementInterface {

    /**
     * @var Collection<int, Evenement>
     */
    #[ORM\OneToMany(targetEntity: Evenement::class, mappedBy: 'type')]
    private Collection $evenements;

    public function __construct() {
        $this->evenements = new ArrayCollection();
    }

    /**
     * @return Collection<int, Evenement>
     */
    public function getEvenements(): Collection {
        return $this->evenements;
    }

    public function addEvenement(Evenement $evenement): static {
        if (!$this->evenements->contains($evenement)) {
            $this->evenements->add($evenement);
            $evenement->setType($this);
        }

        return $this;
    }

    public function removeEvenement(Evenement $evenement): static {
        if ($this->evenements->removeElement($evenement)) {
            // set the owning side to null (unless already changed)
            if ($evenement->getType() === $this) {
                $evenement->setType(null);
            }
        }

        return $this;
    }
}
