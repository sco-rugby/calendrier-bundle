<?php

namespace ScoRugby\CalendrierBundle\Entity;

use ScoRugby\CalendrierBundle\Repository\CalendrierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use ScoRugby\CoreBundle\Entity\AbstractTaxonomie;
use ScoRugby\CoreBundle\Entity\SluggifyInterface;
use ScoRugby\CoreBundle\Entity\Slug;
use ScoRugby\CalendrierBundle\Model\CalendrierInterface;
use ScoRugby\CalendrierBundle\Model\CalendarEventInterface;

#[ORM\Entity(repositoryClass: CalendrierRepository::class)]
//#[ORM\Table(schema: "evenement")]
#[ORM\UniqueConstraint(name: "unq_calendrier_slug", columns: ["slug"])]
class Calendrier extends AbstractTaxonomie implements CalendrierInterface, SluggifyInterface {

    private Slug $slug;

    /**
     * @var Collection<int, Groupe>
      private Collection $groupes;

      /**
     * @var Collection<int, Evenement>
     */
    private Collection $evenements;

    public function __construct() {
        parent::__construct();
        $this->groupes = new ArrayCollection();
        $this->evenements = new ArrayCollection();
        $this->slug = new Slug();
    }

    public function getSlug(): Slug {
        return $this->slug;
    }

    /**
     * @return Collection<int, Groupe>
     */
    public function getGroupes(): Collection {
        return $this->groupes;
    }

    public function addGroupe(Groupe $groupe): static {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes->add($groupe);
            $groupe->setCalendrier($this);
        }

        return $this;
    }

    public function removeGroupe(Groupe $groupe): static {
        if ($this->groupes->removeElement($groupe)) {
            // set the owning side to null (unless already changed)
            if ($groupe->getCalendrier() === $this) {
                $groupe->setCalendrier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Evenement>
     */
    public function getEvenements(): Collection {
        return $this->evenements;
    }

    public function addEvenement(CalendarEventInterface $evenement): static {
        if (!$this->evenements->contains($evenement)) {
            $this->evenements->add($evenement);
            $evenement->setCalendrier($this);
        }

        return $this;
    }

    public function removeEvenement(CalendarEventInterface $evenement): static {
        if ($this->evenements->removeElement($evenement)) {
            // set the owning side to null (unless already changed)
            if ($evenement->getCalendrier() === $this) {
                $evenement->setCalendrier(null);
            }
        }

        return $this;
    }
}
