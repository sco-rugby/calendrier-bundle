<?php

namespace ScoRugby\CalendrierBundle;

use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

/**
 * Description of ScoRugbyCalendrierBundle
 *
 * @author Antoine BOUET
 */
class ScoRugbyCalendrierBundle extends AbstractBundle {
    
}
