<?php

namespace ScoRugby\CalendrierBundle\Manager;

use ScoRugby\CoreBundle\Manager\AbstractDispatchingManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ScoRugby\CalendrierBundle\Repository\EvenementRepository;

final class EvenementManager extends AbstractDispatchingManager {

    public function __construct(EvenementRepository $repository, ?EventDispatcherInterface $dispatcher = null, ?ValidatorInterface $validator = null, ?FormFactoryInterface $formFactory = null) {
        parent::__construct($repository, $dispatcher, $validator, $formFactory);
    }
}
