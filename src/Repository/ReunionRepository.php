<?php

namespace ScoRugby\CalendrierBundle\Repository;

use ScoRugby\CalendrierBundle\Entity\Reunion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use ScoRugby\CoreBundle\Entity\SluggifyInterface;

/**
 * @extends ServiceEntityRepository<Evenement>
 */
class ReunionRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Reunion::class);
    }

    public function save(Organisation $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Organisation $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findBySlug(string $slug): SluggifyInterface {
        return $this->findBy(['slug' => $slug]);
    }

    public function findByExternalId(string $externalId): ?ImportableInterface {
        return $this->findBy(['externalId' => $externalId]);
    }

    public function findByAppliMaitre(string $appliMaitre): array {
        return $this->findBy(['appliMaitre' => $appliMaitre]);
    }

    public function findByImportedDate(\DateTimeInterface $date): array {
        return $this->findBy(['importedAt' => $date]);
    }
}
