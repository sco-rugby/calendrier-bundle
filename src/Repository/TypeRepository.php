<?php

namespace ScoRugby\CalendrierBundle\Repository;

use ScoRugby\CalendrierBundle\Entity\Type;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use ScoRugby\CoreBundle\Entity\SluggifyInterface;
use ScoRugby\CoreBundle\Repository\SlugRepositoryInterface;

/**
 * @extends ServiceEntityRepository<Type>
 */
class TypeRepository extends ServiceEntityRepository implements SlugRepositoryInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Type::class);
    }

    public function save(Organisation $entity, bool $flush = false): void {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Organisation $entity, bool $flush = false): void {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findBySlug(string $slug): ?SluggifyInterface {
        return $this->findBy(['slug' => $slug]);
    }
}
