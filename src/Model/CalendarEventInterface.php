<?php

namespace ScoRugby\CalendrierBundle\Model;

/**
 *
 * @author Antoine BOUET
 */
interface CalendarEventInterface extends \Stringable {

    public function getType(): ?TypeEvenementInterface;

    public function getLibelle(): ?string;

    public function getDebut(): ?\DateTimeInterface;

    public function getFin(): ?\DateTimeInterface;

    public function isAllday(): ?bool;
}
