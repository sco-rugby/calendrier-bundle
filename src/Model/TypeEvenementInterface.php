<?php

namespace ScoRugby\CalendrierBundle\Model;

use ScoRugby\CoreBundle\Model\TaxonomieInterface;

/**
 *
 * @author Antoine BOUET
 */
interface TypeEvenementInterface extends TaxonomieInterface, \Stringable {
    
}
