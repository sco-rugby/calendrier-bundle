<?php

namespace ScoRugby\CalendrierBundle\Model;

use Doctrine\Common\Collections\Collection;
use ScoRugby\CoreBundle\Model\TaxonomieInterface;

/**
 *
 * @author Antoine BOUET
 */
interface CalendrierInterface extends TaxonomieInterface, \Stringable {

    /**
     * @return Collection<int, Evenement>
     */
    public function getEvenements(): Collection;

    public function addEvenement(CalendarEventInterface $evenement): static;

    public function removeEvenement(CalendarEventInterface $evenement): static;
}
