<?php

namespace ScoRugby\CalendrierBundle\Model;

enum TypeEvenement: string implements TypeEvenementInterface {

    case REUNION = 'reunion';
    case MATCH_DOMICILE = 'domicile';
    case MATCH_DEPLACEMENT = 'deplacement';
    case ENTRAINEMENT = 'entrainement';

    public function getId(): ?string {
        return $this->name;
    }

    public function setId(string $id): self {
        return $this;
    }

    public function getLibelle(): ?string {
        return match ($this) {
            TypeEvenement::REUNION => 'Réunion',
            TypeEvenement::MATCH_DOMICILE => 'Match à domicile',
            TypeEvenement::MATCH_DEPLACEMENT => 'Déplacement',
            TypeEvenement::ENTRAINEMENT => 'Entrainement',
        };
    }

    public function setLibelle(string $libelle): self {
        return $this;
    }

    public function getSlug(): ?string {
        return $this->value;
    }

    public function setSlug(string $slug): self {
        return $this;
    }

    public function getDescription(): ?string {
        return '';
    }

    public function setDescription(?string $description): self {
        return $this;
    }
}
