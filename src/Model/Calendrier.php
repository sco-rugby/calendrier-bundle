<?php

namespace ScoRugby\CalendrierBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ScoRugby\CoreBundle\Model\TaxonomyTrait;

class Calendrier implements CalendrierInterface {

    use TaxonomyTrait;

    private $id = null;
    private ?string $libelle = null;
    private ?string $slug = null;
    private ?string $description = null;
    private Collection $evenements;

    public function __construct() {
        $this->evenements = new ArrayCollection();
    }

    /**
     * @return Collection<int, Evenement>
     */
    public function getEvenements(): Collection {
        return $this->evenements;
    }

    public function addEvenement(CalendarEventInterface $evenement): static {
        if (!$this->evenements->contains($evenement)) {
            $this->evenements->add($evenement);
            $evenement->setCalendrier($this);
        }

        return $this;
    }

    public function removeEvenement(CalendarEventInterface $evenement): static {
        if ($this->evenements->removeElement($evenement)) {
            // set the owning side to null (unless already changed)
            if ($evenement->getCalendrier() === $this) {
                $evenement->setCalendrier(null);
            }
        }

        return $this;
    }

    public function __toString(): string {
        return (string) $this->getLibelle();
    }
}
