<?php

namespace ScoRugby\CalendrierBundle\Model;

class Evenement implements CalendarEventInterface, \Stringable {

    private ?int $id = null;
    private ?string $libelle = null;
    private ?string $description = null;
    private ?Type $type = null;
    private ?\DateTimeInterface $debut = null;
    private ?\DateTimeInterface $fin = null;
    private ?bool $indicJournee = false;

    public function getId(): ?int {
        return $this->id;
    }

    public function getLibelle(): ?string {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self {
        $this->libelle = $libelle;

        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(?string $description): self {
        $this->description = $description;

        return $this;
    }

    public function getType(): ?TypeEvenementInterface {
        return $this->type;
    }

    public function setType(?TypeEvenementInterface $type): static {
        $this->type = $type;

        return $this;
    }

    public function getDebut(): ?\DateTimeInterface {
        return $this->debut;
    }

    public function setDebut(\DateTimeInterface $debut): static {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeInterface {
        return $this->fin;
    }

    public function setFin(\DateTimeInterface $fin): static {
        $this->fin = $fin;

        return $this;
    }

    public function isAllday(): ?bool {
        return $this->indicJournee;
    }

    public function setAllday(bool $bool = true): static {
        $this->indicJournee = $bool;

        return $this;
    }

    public function __toString(): string {
        return (string) $this->getLibelle();
    }
}
