<?php

namespace ScoRugby\CalendrierBundle\Model;

use ScoRugby\CoreBundle\Model\TaxonomyTrait;

class Type implements TypeEvenementInterface {

    use TaxonomyTrait;

    private ?string $id = null;
    private ?string $libelle = null;
    private ?string $slug = null;
    private ?string $description = null;
}
